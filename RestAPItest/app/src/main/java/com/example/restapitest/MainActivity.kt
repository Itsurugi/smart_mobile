package com.example.restapitest

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.TextView
import com.android.volley.Request
import com.android.volley.Response
import com.android.volley.toolbox.StringRequest
import com.android.volley.toolbox.Volley
import kotlinx.android.synthetic.main.activity_main.*
import okhttp3.*
import org.json.JSONException
import org.json.JSONObject
import java.io.IOException
import okhttp3.HttpUrl
import android.R.string
import okhttp3.RequestBody
import okhttp3.OkHttpClient
import okhttp3.FormBody
import okhttp3.MultipartBody
import android.os.AsyncTask.execute
import com.google.gson.JsonParser
import com.google.gson.JsonObject













class MainActivity : AppCompatActivity() {




    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        btnLogin.setOnClickListener{
            //FetchPostJSON()
            fetchjson()
        }


    }

    fun fetchjson(){
        println("attempting to fetch json")

        val url = "https://d19fe776.ngrok.io/races"
        val request = okhttp3.Request.Builder().url(url).build()
        val client = OkHttpClient()
        client.newCall(request).enqueue(object: Callback{
            override fun onResponse(call: Call, response: okhttp3.Response) {
                val res = response?.body()?.string()
                println(res)
                val jsonObject = JsonParser().parse(res).getAsJsonObject()
            }
            override fun onFailure(call: Call, e: IOException) {
                println(e)
            }


        })

    }

    fun FetchPostJSON(){

        val formBody = FormBody.Builder()
            .add("username", txtUsername.text.toString())
            .addEncoded("password", txtPassword.text.toString())
            .build()
        val url = "https://697a33b7.ngrok.io/login"
        val request = okhttp3.Request.Builder().url(url)
            .addHeader("login", "login")  // to add header data
            .post(formBody)         // for form data

            .build()

        val client = OkHttpClient()
        client.newCall(request).enqueue(object: Callback{
            override fun onResponse(call: Call, response: okhttp3.Response) {
                val res = response?.body()?.string()
                val responsecode = response.code()
                if(responsecode == 200)
                {
                    println(res)

                }
                else if(responsecode == 204){
                    println("failed to login")
                }

            }
            override fun onFailure(call: Call, e: IOException) {
                println(e)
            }
        })
    }

    fun POST(){


    }
}
