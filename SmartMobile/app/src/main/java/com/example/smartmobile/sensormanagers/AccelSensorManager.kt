package com.example.smartmobile.sensormanagers

import android.app.Service
import android.hardware.Sensor
import android.hardware.SensorEvent
import android.hardware.SensorEventListener
import android.hardware.SensorManager
import android.os.Handler
import android.os.HandlerThread
import android.util.Log
import com.example.smartmobile.MyApplication
import com.example.smartmobile.models.MySensorEvent
import com.example.smartmobile.models.SensorType


object AccelSensorManager :
    HandlerThread("AccelSensorManager"), SensorEventListener {
        private val TAG : String = "AccelSensorManager"
        private var handler: Handler? = null //Handler instance with null value
        private var sensorManager : SensorManager?= null //Sensor manager with null value
        private var sensor : Sensor?= null //Sensor instance with null value
        private var sensorExists = false //Bool to check if sensor exists on device
        private var sensorThread: HandlerThread? = null //New thread instance with null value
        private var sensorHandler: Handler? = null //Handler that will read data from sensor

        init{
            sensorManager = (MyApplication.getApplicationContext().getSystemService(Service.SENSOR_SERVICE)) as SensorManager
            sensor = sensorManager!!.getDefaultSensor(Sensor.TYPE_ACCELEROMETER)

            // Check sensor exists
            if (sensor != null) {
                sensorExists = true
            } else {
                sensorExists = false
            }
        }

        fun startSensor(){
            sensorThread = HandlerThread(TAG, Thread.NORM_PRIORITY)
            sensorThread!!.start()
            sensorHandler = Handler(sensorThread!!.getLooper()) //Blocks until looper is prepared, which is fairly quick
            sensorManager!!.registerListener(this,
                sensor, SensorManager.SENSOR_DELAY_NORMAL,
                sensorHandler
            )
        }

        fun stopSensor(){
            sensorManager!!.unregisterListener(this)
            sensorThread!!.quitSafely()
        }

        fun sensorExists() : Boolean{
            return sensorExists
        }

        override fun onAccuracyChanged(sensor: Sensor?, accuracy: Int) {
            Log.d(TAG, ""+accuracy)
        }

        override fun onSensorChanged(event: SensorEvent?) {
            if (event != null && event.values.isNotEmpty()) {

                var msgEvent = MySensorEvent()
                msgEvent.type = SensorType.ACCEL

                // Format event values
                msgEvent.value = event.values[0].toString()

                // Send message to MainActivity
                sendMessage(msgEvent)
            }
        }

        fun setHandler(handler: Handler){
            this.handler = handler
        }

        fun sendMessage(sensorEvent: MySensorEvent) {
            if (handler == null) return

            handler?.obtainMessage(sensorEvent.type.ordinal, sensorEvent)?.apply {
                sendToTarget()
            }
        }
}