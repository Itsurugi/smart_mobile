package com.example.smartmobile.models

enum class SensorType(i: Int) {
    ACCEL(1)
}

class MySensorEvent {
    var type = SensorType.ACCEL
    var value = ""
}