package com.example.smartmobile

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.os.Message
import kotlinx.android.synthetic.main.activity_main.*
import java.util.*
import com.example.smartmobile.models.MySensorEvent
import com.example.smartmobile.models.SensorType
import com.example.smartmobile.sensormanagers.AccelSensorManager

class MainActivity : AppCompatActivity(){

    private var handler2 : Handler? = null

    fun setHandler(handler: Handler){
        this.handler2 = handler
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        AccelSensorManager.setHandler(handler)

        btnRoll.setOnClickListener{
            val intent = Intent(this, StatsActivity::class.java)

            startActivity(intent)
        }
    }

    val random = Random()

    fun rand(from: Int, to: Int) : Int {
        return random.nextInt(to - from) + from
    }

    val handler: Handler = object : Handler(Looper.getMainLooper()) {
        /*
         * handleMessage() defines the operations to perform when
         * the Handler receives a new Message to process.
         */
        override fun handleMessage(inputMessage: Message) {
            // Gets the image task from the incoming Message object.
            val sensorEvent = inputMessage.obj as MySensorEvent

            // Light Sensor events
            if (sensorEvent.type == SensorType.ACCEL){
                //val test1: Int = sensorEvent.value.toInt()
                val test: String = sensorEvent.value
                    if(test.toFloat() > 5) {
                        txtSensor.text = rand(1, 20).toString()
                        AccelSensorManager.stopSensor()
                    }
            }
        }
    }
}
