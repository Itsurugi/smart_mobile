package com.example.smartmobile

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.os.Message
import android.view.MotionEvent
import android.view.View
import com.example.smartmobile.models.MySensorEvent
import com.example.smartmobile.models.SensorType
import com.example.smartmobile.sensormanagers.AccelSensorManager
import kotlinx.android.synthetic.main.activity_stats.*
import java.lang.Math.ceil
import java.lang.Math.floor
import java.util.*
import kotlin.math.ceil
import kotlin.math.floor
import kotlin.math.roundToInt

class StatsActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_stats)

        AccelSensorManager.setHandler(handler)
        addListenerOnButton()
    }

    fun addListenerOnButton(){
        btnRollD20.setOnClickListener{
            GenerateStats()
            //Toast.makeText(applicationContext,"test",Toast.LENGTH_SHORT).show()

        }

        btnRollD20.setOnTouchListener(object : View.OnTouchListener {
            override fun onTouch(v: View?, event: MotionEvent?): Boolean {
                when (event?.action) {
                    MotionEvent.ACTION_UP -> AccelSensorManager.startSensor()//Do Something
                }

                return v?.onTouchEvent(event) ?: true
            }
        })
    }



    fun DisplayStats(x: Int, res: Int, modifier: Int ){
        if(x == 0){
            txtSTRRoll.text = res.toString()
            txtSTRMod.text = getString(R.string.mod, modifier)
            txtSTRSave.text = getString(R.string.savingthrow, modifier)
        }
        else if(x == 1){
            txtDEXRoll.text = res.toString()
            txtDEXMod.text = getString(R.string.mod, modifier)
            txtDEXSave.text = getString(R.string.savingthrow, modifier)
        }
        else if(x == 2){
            txtCONSRoll.text = res.toString()
            txtCONSMod.text = getString(R.string.mod, modifier)
            txtCONSSave.text = getString(R.string.savingthrow, modifier)
        }
        else if(x == 3){
            txtINTRoll.text = res.toString()
            txtINTMod.text = getString(R.string.mod, modifier)
            txtINTSave.text = getString(R.string.savingthrow, modifier)
        }
        else if(x == 4){
            txtWISRoll.text = res.toString()
            txtWISMod.text = getString(R.string.mod, modifier)
            txtWISSave.text = getString(R.string.savingthrow, modifier)
        }
        else if(x == 5){
            txtCHARoll.text = res.toString()
            txtCHAMod.text = getString(R.string.mod, modifier)
            txtCHASave.text = getString(R.string.savingthrow, modifier)
        }
    }


    fun GenerateStats(){
        var rolls: MutableList<Int> = mutableListOf()
            for(x in 0 until 6 step 1){
                rolls.clear()
                for(y in 0 until 4 step 1){

                    rolls.add(rand(1,6))

                }
                rolls.sortDescending()
                rolls.removeAt(3)

                val total: Int = rolls.sum() - 10
                val modifier = floor(total.toFloat()/ 2)

                DisplayStats(x, rolls.sum(), modifier.toInt())
            }
            AccelSensorManager.stopSensor()

    }

    val random = Random()

    fun rand(from: Int, to: Int) : Int {
        return random.nextInt(to - from) + from
    }



    val handler: Handler = object : Handler(Looper.getMainLooper()) {
        /*
         * handleMessage() defines the operations to perform when
         * the Handler receives a new Message to process.
         */
        override fun handleMessage(inputMessage: Message) {
            // Gets the image task from the incoming Message object.
            val sensorEvent = inputMessage.obj as MySensorEvent

            // Accel Sensor events
            if (sensorEvent.type == SensorType.ACCEL){
                //val test1: Int = sensorEvent.value.toInt()
                //val test: String = sensorEvent.value
                if(sensorEvent.value.toFloat() > 5) {
                    GenerateStats()
                }
            }
        }
    }
}
