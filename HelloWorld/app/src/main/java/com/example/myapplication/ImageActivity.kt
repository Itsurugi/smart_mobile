package com.example.myapplication

import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.Color
import android.location.Location
import android.location.LocationListener
import android.location.LocationManager
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.telephony.SmsManager
import android.util.Log
import android.widget.ImageView
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import kotlinx.android.synthetic.main.activity_image.*
import java.util.jar.Manifest

class ImageActivity : AppCompatActivity() {
    private var locationManager : LocationManager? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_image)
        val MobileNumber = "0628366545"
        val Message = "test"
        val smgr = SmsManager.getDefault()
        locationManager = getSystemService(LOCATION_SERVICE) as LocationManager?;

        val locationListener: LocationListener = object : LocationListener {
            override fun onLocationChanged(location: Location) {
                txtLocation.setText("Longtitude " + location.longitude + " Latitude " + location.latitude);
            }
            override fun onStatusChanged(provider: String, status: Int, extras: Bundle) {}
            override fun onProviderEnabled(provider: String) {}
            override fun onProviderDisabled(provider: String) {}
        }
        btnGetLocation.setOnClickListener { view ->
            try {
                // Request location updates
                locationManager?.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 0L, 0f, locationListener);

            } catch(ex: SecurityException) {
                Log.d("myTag", "Security Exception, no location available");
            }


        }

        btnBack.setOnClickListener{
            val intent = Intent(this, MainActivity::class.java)
            startActivity(intent)
        }

        btnChangeToDodo.setOnClickListener{
            imageView3.setImageResource(R.drawable.dodo)
            ImageLayout.setBackgroundColor(Color.parseColor("#add8e6"))
        }
        btnChangeToBull.setOnClickListener{
            imageView3.setImageResource(R.drawable.monster)
            ImageLayout.setBackgroundColor(Color.parseColor("#CD853F"))
        }
        btnChangeToGriffy.setOnClickListener{



            //imageView3.setImageResource(R.drawable.griffon)
            //ImageLayout.setBackgroundColor(Color.parseColor("#FFFFE0"))

            smgr.sendTextMessage(MobileNumber, null, Message, null, null)

        }
        btnPermission.setOnClickListener{
            val permissions = arrayOf(android.Manifest.permission.SEND_SMS)
            ActivityCompat.requestPermissions(this, permissions,0)
            val permissions1 = arrayOf(android.Manifest.permission.ACCESS_COARSE_LOCATION)
            ActivityCompat.requestPermissions(this, permissions1,0)
            val permissions2 = arrayOf(android.Manifest.permission.ACCESS_FINE_LOCATION)
            ActivityCompat.requestPermissions(this, permissions2,0)
        }
        /*
        val imageView = findViewById<ImageView>(R.id.imageView)

        val imgResId = R.drawable.monster
        var resId = imgResId
        imageView.setImageResource(imgResId)
        */

        btnBack.setOnClickListener{
            val intent = Intent(this, MainActivity::class.java)
            val text = txtLocation.text;
            intent.putExtra("location",text)
            startActivity(intent)
        }

    }
}
