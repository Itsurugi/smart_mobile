package com.example.myapplication

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        if(intent.getStringExtra("location") != null){
            var strUser: String = intent.getStringExtra("location")
            txtLoc.setText(strUser)
        }

        btnImage.setOnClickListener{
            val intent = Intent(this, ImageActivity::class.java)
            startActivity(intent)
        }

        btnSensor.setOnClickListener{
            val intent = Intent(this, SensorActivity::class.java)

            startActivity(intent)
        }
    }

}

